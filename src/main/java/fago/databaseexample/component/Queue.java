package fago.databaseexample.component;

import fago.databaseexample.entries.Playlist;
import fago.databaseexample.entries.Song;
import fago.databaseexample.repositories.PlaylistRepository;
import fago.databaseexample.repositories.SongRepository;
import lombok.Getter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Getter
public class Queue {
    private PlaylistRepository playlistRepository;
    private SongRepository songRepository;

    public Queue(PlaylistRepository repository, SongRepository songRepository){
        this.playlistRepository = repository;
        this.songRepository = songRepository;
    }

    @PostConstruct
    private void init(){
        List<Playlist> playlists = playlistRepository.findByName("Queue");
        if (playlists.isEmpty()) {
            playlistRepository.save(new Playlist("Queue"));
        }
    }

    public void addEntry(Song song) {
        Playlist queue = playlistRepository.findByName("Queue").get(0);
        queue.addSong(song);
        playlistRepository.save(queue);
    }

    @Transactional
    public void addEntry(String songName) {
        Song song = new Song(songName);
        song = songRepository.save(song);
        addEntry(song);
    }
}
