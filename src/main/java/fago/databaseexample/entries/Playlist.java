package fago.databaseexample.entries;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@NoArgsConstructor
public class Playlist {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToMany
    @JoinTable(
            name = "playlist_song",
            joinColumns = @JoinColumn(name = "playlist_id"),
            inverseJoinColumns = @JoinColumn(name = "song_id"))
    private List<Song> entries;

    public Playlist(String name) {
        this.name = name;
        this.entries = new ArrayList<>();
    }

    public void addSong(Song song) {
        entries.add(song);
        song.getPlaylists().add(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Playlist playlist = (Playlist) obj;

        return id.equals(playlist.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        StringBuilder allEntriesToString = new StringBuilder();
        allEntriesToString.append("{");
        for(Song e: entries){
            allEntriesToString.append(" ").append(e.getSongName()).append(";");
        }
        allEntriesToString.append("}");
        return "Playlist: [name=" + name + ", size: " + entries.size() + ", entries: " + allEntriesToString + "]";
    }
}
