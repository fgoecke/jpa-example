package fago.databaseexample.controller;

import fago.databaseexample.component.Queue;
import fago.databaseexample.entries.Playlist;
import fago.databaseexample.repositories.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {
    @Autowired
    Queue queue;

    @Autowired
    PlaylistRepository playlistRepository;

    @GetMapping(path = "/addSong")
    public void addSong(String songName) {
        queue.addEntry(songName);
    }

    @GetMapping(path = "/getAllPlaylists")
    public List<Playlist> getAllPlaylistNames() {
        return playlistRepository.findAll();
    }
}
