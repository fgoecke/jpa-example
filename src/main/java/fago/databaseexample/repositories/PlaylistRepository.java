package fago.databaseexample.repositories;

import fago.databaseexample.entries.Playlist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlaylistRepository extends CrudRepository<Playlist, Long> {
    List<Playlist> findByName(String name);

    Optional<Playlist> findById(Long id);

    List<Playlist> findAll();
}
